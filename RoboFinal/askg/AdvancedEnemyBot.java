package askg;

import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.awt.geom.Point2D;
import java.util.Arrays;


/**
 * Record the advanced state of an enemy bot.
 *
 * @author Alex Shmakov
 * @author Period - 1
 * @author Assignment - AdvancedEnemyBot
 * @author Sources - None
 * @version 5-12-14
 */
public class AdvancedEnemyBot extends EnemyBot {

    private double fieldWidth;
    private double fieldHeight;

    private double x;
    private double y;

    private double absoluteBearing;

    private double previousVel;
    private double accel;

    private boolean alive = true;

    /**
     * Constructor
     */
    public AdvancedEnemyBot() {
        this.reset();
    }


    /**
     * Accesser method for X.
     *
     * @return double x
     */
    public double getX() {
        return x;
    }


    /**
     * Accesser method for X.
     *
     * @return double y
     */
    public double getY() {
        return y;
    }

    public double getAccel() {
        return accel;
    }

    public double getWallDistance()
    {
        double[] distances = new double[4];
        distances[0] = getPosition().getX();
        distances[1] = getPosition().getY();
        distances[2] = fieldWidth - distances[0];
        distances[3] = fieldHeight - distances[1];

        Arrays.sort(distances);

        return distances[0];
    }

    public Point2D.Double getPosition() {
        return new Point2D.Double(x, y);
    }

    public double getAbsoluteBearing() {

        return absoluteBearing;
    }

    /**
     * Updates robots x and y values.
     *
     * @param e     ScannedEvent
     * @param robot robot
     */
    public void update(ScannedRobotEvent e, Robot robot) {
        super.update(e);

        fieldHeight = robot.getBattleFieldHeight();
        fieldWidth = robot.getBattleFieldWidth();

        absoluteBearing = (robot.getHeading() + e.getBearing());
        while (absoluteBearing < 0) {
            absoluteBearing += 360;
        }

        accel = previousVel - getVelocity();
        previousVel = getVelocity();

        x = robot.getX() + Math.sin(Math.toRadians(absoluteBearing))
                * e.getDistance();
        y = robot.getY() + Math.cos(Math.toRadians(absoluteBearing))
                * e.getDistance();

    }


    /**
     * Calculates future x value of robot.
     *
     * @param when what time to calculate.
     * @return future x
     */
    public double getFutureX(long when) {
        return x + Math.sin(Math.toRadians(getHeading())) * getVelocity()
                * when;
    }


    /**
     * Calculates future y value of robot.
     *
     * @param when what time to calculate.
     * @return future y
     */
    public double getFutureY(long when) {
        return y + Math.cos(Math.toRadians(getHeading())) * getVelocity()
                * when;
    }

    /*
     * (non-Javadoc)
     * 
     * @see askg.EnemyBot#reset()
     */
    public void reset() {
        super.reset();
        x = 0.0;
        y = 0.0;
        alive = false;
    }

}