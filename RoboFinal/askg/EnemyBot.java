package askg;

import robocode.ScannedRobotEvent;


/**
 * Record the state of an enemy bot.
 *
 * @author Alex Shmakov
 * @author Period - 1
 * @author Assignment - EnemyBot
 * @author Sources - None
 * @version 12-5-14
 */
public class EnemyBot {
    private double bearing;

    private double distance;

    private double energy;

    private double heading;

    private double velocity;

    private String name;


    /**
     * Class for storing enemy robot information.
     */
    public EnemyBot() {
        bearing = 0.0;
        distance = 0.0;
        energy = 0.0;
        heading = 0.0;
        velocity = 0.0;
        name = "";
    }


    /**
     * Get bearing with respect to the robot.
     *
     * @return Bearing in degrees.
     */
    public double getBearing() {
        return bearing;
    }


    /**
     * Get distance with respect to the robot.
     *
     * @return Distance between enemy and your robot's centers.
     */
    public double getDistance() {
        return distance;
    }


    /**
     * Get energy level of enemy robot.
     *
     * @return Energy level.
     */
    public double getEnergy() {
        return energy;
    }


    /**
     * Gets current heading of enemy robot.
     *
     * @return Heading in degrees.
     */
    public double getHeading() {
        return heading;
    }


    /**
     * Gets current velocity of enemy robot.
     *
     * @return Velocity.
     */
    public double getVelocity() {
        return velocity;
    }


    /**
     * Gets current name of enemy robot.
     *
     * @return Robot's name.
     */
    public String getName() {
        return name;
    }


    /**
     * Update enemy root information with that in the event.
     *
     * @param srEvt Latest event.
     */
    public void update(ScannedRobotEvent srEvt) {
        bearing = srEvt.getBearing();
        distance = srEvt.getDistance();
        energy = srEvt.getEnergy();
        heading = srEvt.getHeading();
        velocity = srEvt.getVelocity();
        name = srEvt.getName();
    }


    /**
     * Resets enemy robot to default values.
     */
    public void reset() {
        bearing = 0.0;
        distance = 0.0;
        energy = 0.0;
        heading = 0.0;
        velocity = 0.0;
        name = "";
    }


    /**
     * Checks to see if enemy robot is defined.
     *
     * @return true if not defined.
     */
    public boolean none() {
        return name.length() == 0;
    }
}