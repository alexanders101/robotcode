package askg;

import robocode.util.Utils;

import java.awt.geom.Point2D;

public class WaveBullet {
    private final double startBearing;
    private final double power;
    private final double lateralVelocity;
    private final Point2D.Double startPosition;
    private final long fireTime;
    private double[] dataPoint;
    private AdvancedEnemyBot enemy;

    public WaveBullet(AdvancedEnemyBot e, double x, double y, double bearing, double power, double latVel,
                      long time, double[] datapoint) {
        enemy = e;
        startPosition = new Point2D.Double(x, y);
        startBearing = bearing;
        lateralVelocity = latVel;
        this.power = power;
        fireTime = time;
        dataPoint = datapoint;
    }
    

    public double[] getDataPoint()
    {
        return dataPoint;
    }
    
    public static double bulletVelocity(double bPower) {
        return 20.0 - (3.0 * bPower);
    }

//    private static double maxTheoEscapeAngle(double bPower) {
//        return Math.asin(8 / bulletVelocity(bPower));
//    }

    public static double calcEscapeAngle(Point2D.Double startPosition, double bPower, AdvancedEnemyBot enemy, double lateralVelocity, double turn)
    {
        double headingA = enemy.getHeading();
        double headingB = enemy.getHeading();
        Simulate startState = new Simulate(enemy.getPosition(), Math.toRadians(/*PartsBot.calcRelativeHeading(heading, enemy.getHeading())*/enemy.getHeading()), enemy.getVelocity(), 0, Math.toRadians(turn), calcSign(lateralVelocity));
        Simulate endState = startState.copy();

        endState.multiStep(startPosition, bulletVelocity(bPower));

        double angle = lawOfCosines(startPosition, startState.position, endState.position);

        return angle;
    }

    public static double calcEscapeAngle2(Point2D.Double startPosition, double bPower, AdvancedEnemyBot enemy, double lateralVelocity, double turn)
    {

        MovementPredictor.PredictionStatus startState = new MovementPredictor.PredictionStatus(enemy.getX(), enemy.getY(), Math.toRadians(enemy.getHeading()), enemy.getVelocity(), 0);
        MovementPredictor.PredictionStatus endState = new MovementPredictor.PredictionStatus(enemy.getX(), enemy.getY(), Math.toRadians(enemy.getHeading()), enemy.getVelocity(), 0);

        Point2D.Double startPoint = new Point2D.Double(startState.getX(), startState.getY());
        Point2D.Double endPoint = new Point2D.Double(endState.getX(), endState.getY());
        double bSpeed = bulletVelocity(bPower);
        int tick = 0;
        while (startPosition.distance(endPoint) >  bSpeed * tick)
        {
            endState = MovementPredictor.predict(endState, Math.toRadians(turn));
            endPoint.setLocation(endState.getX(), endState.getY());
            tick++;
        }

        double angle = lawOfCosines(startPosition, startPoint, endPoint);

        return angle;
    }

    public static double lawOfCosines(Point2D.Double source, Point2D.Double point1, Point2D.Double point2)
    {
        double a = source.distance(point1);
        double b = source.distance(point2);
        double c = point1.distance(point2);

        double angle = Math.acos((Math.pow(c, 2) - Math.pow(b, 2) - Math.pow(a, 2)) / (-2.0 * a * b));

        return angle;
    }

    public static byte calcSign(double input) {
        if (input >= 0)
            return 1;
        else
            return -1;
    }

    public static double calcGFFiringAngle(double gFactor, Point2D.Double startPosition, double bPower, AdvancedEnemyBot enemy, double lateralVelocity, double turn) {
        return gFactor * calcEscapeAngle2(startPosition, bPower, enemy, lateralVelocity, turn);
    }

    private double bulletVelocity() {
        return 20.0 - (3.0 * power);
    }

//    private double maxTheoEscapeAngle() {
//        return Math.asin(8 / bulletVelocity());
//    }

    public boolean checkBullet(Point2D enemyPosition, long time) {

        return (startPosition.distance(enemyPosition) <= ((time - fireTime) * bulletVelocity()));

    }

    public double calcGuessFactor(Point2D enemyPosition, double turn) {
        double hitAngle = LRGFBot.calcAngle(enemyPosition, startPosition);
        double bearingOffset = Utils.normalRelativeAngle(hitAngle - startBearing);
        double latDirection = calcSign(lateralVelocity);
        return LRGFBot.limit(-1.1, latDirection * bearingOffset / calcEscapeAngle2(startPosition, power, enemy, lateralVelocity, turn), 1.1);
    }


}
    
